# Log Management: OpenSearch

```shell
hcloud server create --image rocky-8 --type cx21 --name ix-log-opensearch

curl -SL https://artifacts.opensearch.org/releases/bundle/opensearch/2.x/opensearch-2.x.repo -o /etc/yum.repos.d/opensearch-2.x.repo
curl -SL https://artifacts.opensearch.org/releases/bundle/opensearch-dashboards/2.x/opensearch-dashboards-2.x.repo -o /etc/yum.repos.d/opensearch-dashboards-2.x.repo
yum clean all

yum -y install opensearch opensearch-dashboards

vi /etc/opensearch-dashboards/opensearch_dashboards.yml

server.host: "65.21.0.6"

systemctl start opensearch
systemctl start opensearch-dashboards

curl -XGET https://localhost:9200 -u 'admin:admin' --insecure
{
  "name" : "ix-log-opensearch",
  "cluster_name" : "opensearch",
  "cluster_uuid" : "TqLhzhqXSEOrfjPaZvcfzA",
  "version" : {
    "distribution" : "opensearch",
    "number" : "2.1.0",
    "build_type" : "rpm",
    "build_hash" : "388c80ad94529b1d9aad0a735c4740dce2932a32",
    "build_date" : "2022-06-30T21:31:20.473786012Z",
    "build_snapshot" : false,
    "lucene_version" : "9.2.0",
    "minimum_wire_compatibility_version" : "7.10.0",
    "minimum_index_compatibility_version" : "7.0.0"
  },
  "tagline" : "The OpenSearch Project: https://opensearch.org/"
}
```

```shell
# Filebeat
dnf install -y https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-oss-7.12.1-x86_64.rpm

vi /etc/filebeat/filebeat.yml

- type: filestream

  # Change to true to enable this input configuration.
  enabled: true

  # Paths that should be crawled and fetched. Glob based paths.
  paths:
    - /var/log/*.log
    #- c:\programdata\elasticsearch\logs\*

output.elasticsearch:
  # Array of hosts to connect to.
  hosts: ["localhost:9200"]

  # Protocol - either `http` (default) or `https`.
  protocol: "https"

  # Authentication credentials - either API key or username/password.
  #api_key: "id:api_key"
  username: "admin"
  password: "admin"


systemctl start filebeat       
```
