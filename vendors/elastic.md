# Log Management: Elastic

```shell
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-elastic

ssh root@...

# https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html 
apt-get update && apt install -y openjdk-8-jdk apt-transport-https 

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list

apt-get update && apt -y install elasticsearch

--------------------------- Security autoconfiguration information ------------------------------

Authentication and authorization are enabled.
TLS for the transport and HTTP layers is enabled and configured.

The generated password for the elastic built-in superuser is : XXX

systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl start elasticsearch.service

curl --cacert /etc/elasticsearch/certs/http_ca.crt -u elastic https://localhost:9200

Enter host password for user 'elastic':

{
  "name" : "ix-log-elastic",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "b3tiClRGT7KgCWtcuCZAuA",
  "version" : {
    "number" : "8.3.2",
    "build_type" : "deb",
    "build_hash" : "8b0b1f23fbebecc3c88e4464319dea8989f374fd",
    "build_date" : "2022-07-06T15:15:15.901688194Z",
    "build_snapshot" : false,
    "lucene_version" : "9.2.0",
    "minimum_wire_compatibility_version" : "7.17.0",
    "minimum_index_compatibility_version" : "7.0.0"
  },
  "tagline" : "You Know, for Search"
}

# Kibana 
# https://www.elastic.co/guide/en/kibana/current/deb.html
apt install -y kibana 

vim /etc/kibana/kibana.yml

server.port: 5601
server.host: "65.108.241.97"

systemctl daemon-reload
systemctl enable kibana.service
systemctl start kibana.service 

/usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana

/usr/share/kibana/bin/kibana-verification-code
Your verification code is:  123 456

# Elastic Agent
curl -L -O https://artifacts.elastic.co/downloads/beats/elastic-agent/elastic-agent-8.3.2-amd64.deb
dpkg -i elastic-agent-8.3.2-amd64.deb 
systemctl enable elastic-agent 
systemctl start elastic-agent

vim /etc/elastic-agent/elastic-agent.yml 

```
