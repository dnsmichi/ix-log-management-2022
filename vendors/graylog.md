# Log Management: Graylog

```shell 
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-graylog

# password_secret
pwgen -N 1 -s 96
generatedstring1

# root_password_sha2
echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1
<enter password>
generatedstring2

vim /etc/graylog/server/server.conf

password_secret = generatedstring1

root_password_sha2 = generatedstring2 

http_bind_address = 65.21.0.6:9000

systemctl restart graylog-server 

less /var/log/graylog/server.log 
2022-07-13T18:40:21.940Z INFO  [ServerBootstrap] Graylog server up and running.

# Elastic Filebeat als Log-Lieferant 
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install -y apt-transport-https
echo "deb https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-8.x.list

sudo apt-get update && sudo apt-get install -y filebeat
sudo systemctl enable filebeat

vim /etc/filebeat/filebeat.yml

# filestream is an input for collecting log messages from files.
- type: filestream

  # Unique ID among all inputs, an ID is required.
  id: my-filestream-id

  # Change to true to enable this input configuration. ## Wichtig: Ist per default ausgeschaltet. 
  enabled: true 

  # Paths that should be crawled and fetched. Glob based paths.
  paths:
    - /var/log/*.log
    - /var/log/graylog/*.log
    - /var/log/syslog
    #- c:\programdata\elasticsearch\logs\*

#output.elasticsearch:
  # Array of hosts to connect to.
  #  hosts: ["localhost:9200"]

output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

# Config-Check
filebeat -c /etc/filebeat/filebeat.yml

systemctl restart filebeat 
```
