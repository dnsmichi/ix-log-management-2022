# Log Management: Datadog 

```shell 
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-datadog

DD_AGENT_MAJOR_VERSION=7 DD_API_KEY=XXX DD_SITE="datadoghq.eu" bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script.sh)"

vim /etc/datadog-agent/datadog.yaml

logs_enabled: true

usermod -a -G systemd-journal dd-agent

cat >/etc/datadog-agent/conf.d/journald.d/conf.yaml<<EOF
logs:
  - type: journald
    path: /var/log/journal
    include_units:
      - kernel.service
      - docker.service
EOF

usermod -a -G adm dd-agent

mkdir -p /etc/datadog-agent/conf.d/syslog.d/
cat >/etc/datadog-agent/conf.d/syslog.d/conf.yaml<<EOF
#Log section
logs:

    # - type : (mandatory) type of log input source (tcp / udp / file)
    #   port / path : (mandatory) Set port if type is tcp or udp. Set path if type is file
    #   service : (mandatory) name of the service owning the log
    #   source : (mandatory) attribute that defines which integration is sending the log
    #   sourcecategory : (optional) Multiple value attribute. Can be used to refine the source attribute
    #   tags: (optional) add tags to each log collected

  - type: file
    path: /var/log/syslog
    service: syslog
    source: syslog
EOF 

apt -y install nginx 

vim /etc/nginx/sites-enabled/default

server {
  //
        location /nginx_status {
                stub_status;
        }
}

systemctl restart nginx


cp /etc/datadog-agent/conf.d/nginx.d/conf.yaml.example /etc/datadog-agent/conf.d/nginx.d/conf.yaml

vim /etc/datadog-agent/conf.d/nginx.d/conf.yaml

instances:
  - nginx_status_url: http://localhost/nginx_status/

cat >>/etc/datadog-agent/conf.d/nginx.d/conf.yaml<<EOF
#Log section
logs:

    # - type : (mandatory) type of log input source (tcp / udp / file)
    #   port / path : (mandatory) Set port if type is tcp or udp. Set path if type is file
    #   service : (mandatory) name of the service owning the log
    #   source : (mandatory) attribute that defines which integration is sending the log
    #   sourcecategory : (optional) Multiple value attribute. Can be used to refine the source attribute
    #   tags: (optional) add tags to each log collected

  - type: file
    service: myservice
    path: /var/log/nginx/access.log
    source: nginx
    sourcecategory: http_web_access

  - type: file
    service: myservice
    path: /var/log/nginx/error.log
    source: nginx
    sourcecategory: http_web_access
EOF


systemctl restart datadog-agent
```
