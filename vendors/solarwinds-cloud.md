# Log Management: SolarWinds Cloud 

```shell
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-solarwinds

curl https://files.solarwinds.cloud/solarwinds-snap-agent-installer.sh -o solarwinds-snap-agent-installer.sh
chmod +x solarwinds-snap-agent-installer.sh

sudo bash -x ./solarwinds-snap-agent-installer.sh --token XXX

mkdir -p /etc/apt/keyrings/
curl -fsSL https://packagecloud.io/solarwinds/swisnap/gpgkey | gpg --dearmor > /etc/apt/keyrings/solarwinds_swisnap-archive-keyring.gpg
apt-get update 

sudo bash -x ./solarwinds-snap-agent-installer.sh --token XXX --detect-logs 

# /var/log Lese-Berechtigung
sudo usermod -a -G adm solarwinds

# Einrichtung des Log-Sammlers geschieht via Tasks, anders als im Frontend suggiert
# https://documentation.solarwinds.com/en/success_center/loggly/content/admin/ao-integration-setup.htm#Files 
cp /opt/SolarWinds/Snap/etc/tasks-autoload.d/task-logs-files.yaml.example /opt/SolarWinds/Snap/etc/tasks-autoload.d/task-logs-files.yaml

vim /opt/SolarWinds/Snap/etc/tasks-autoload.d/task-logs-files.yaml

      file_paths:
        - /var/log/syslog
        - /var/log/messages

chown solarwinds:solarwinds /opt/SolarWinds/Snap/etc/tasks-autoload.d/task-logs-files.yaml

service swisnapd restart
less /var/log/SolarWinds/Snap/swisnapd.log
shift +f 

# Alternative: Loggly Linux Log File
wget https://raw.githubusercontent.com/loggly/install-script/master/Linux%20Script/configure-linux.sh
chmod +x configure-linux.sh
./configure-linux.sh -a accountname -u username -t customer-token

SUCCESS: Verification logs successfully transferred to Loggly! You are now sending Linux system logs to Loggly.
```
