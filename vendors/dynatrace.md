# Log Management: Dynatrace

```shell
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-dynatrace

wget  -O Dynatrace-OneAgent-Linux-1.243.166.sh "https://xxx.live.dynatrace.com/api/v1/deployment/installer/agent/unix/default/latest?arch=x86&flavor=default" --header="Authorization: Api-Token xxx"

wget https://ca.dynatrace.com/dt-root.cert.pem ; ( echo 'Content-Type: multipart/signed; protocol="application/x-pkcs7-signature"; micalg="sha-256"; boundary="--SIGNED-INSTALLER"'; echo ; echo ; echo '----SIGNED-INSTALLER' ; cat Dynatrace-OneAgent-Linux-1.243.166.sh ) | openssl cms -verify -CAfile dt-root.cert.pem > /dev/null

/bin/sh Dynatrace-OneAgent-Linux-1.243.166.sh --set-infra-only=false --set-app-log-content-access=true
```
